# Todo list app

This is a simple To-Do List application.The interface is responsive and features a date and time stamp for each task, as well as edit and delete icons.

## Usage

1. Open the index.html file in your web browser.

## Technologies Used

- HTML
- CSS

## Demo
[live demo](https://kcf-html-css-todo-app.netlify.app/)
